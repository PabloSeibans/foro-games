// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'angular-log-in',
    appId: '1:921997698493:web:f80e4b8d6b278bf2405ed4',
    storageBucket: 'angular-log-in.appspot.com',
    apiKey: 'AIzaSyAI5oGxXmE5Dns7p2DbwO05QuDnuLp47ug',
    authDomain: 'angular-log-in.firebaseapp.com',
    messagingSenderId: '921997698493',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
