import { Component } from '@angular/core';
import { AutenticacionService } from './services/autenticacion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'forum-pablo';

  constructor(
    public authService: AutenticacionService, private router: Router
  ){}

  logout(){
    this.authService.logout().subscribe(()=>{
      this.router.navigate(['']);
    });
  }
}
