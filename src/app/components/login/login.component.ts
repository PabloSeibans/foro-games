// import { Component, OnInit } from '@angular/core';
// import { FormGroup, FormControl, Validators } from '@angular/forms';

// @Component({
//   selector: 'app-login',
//   templateUrl: './login.component.html',
//   styleUrls: ['./login.component.css']
// })
// export class LoginComponent implements OnInit {
//   loginForm = new FormGroup({
//     email: new FormControl('',[Validators.required, Validators.email]),
//     password: new FormControl('',[Validators.required])
//   });

//   constructor() { }

//   ngOnInit(): void {
//   }

//   get email() {
//     return this.loginForm.get('email');
//   }

//   get password() {
//     return this.loginForm.get('password');
//   }



//   submit() {
//     if (!this.loginForm.valid) {
//       return;
//     }
//     const { email, password } = this.loginForm.value;
    

//   }
// }



import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AutenticacionService } from '../../services/autenticacion.service';
import { HotToastService } from '@ngneat/hot-toast';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required])
  });

  constructor(private authService: AutenticacionService,
            private router: Router,
            private toast: HotToastService){}


  ngOnInit(): void {
  }

  get email() {
    return this.loginForm.get('email');
  }


  get password() {
    return this.loginForm.get('password');
  }

  submit() {
    if (!this.loginForm.valid) {
      return;
    }
    

    const { email, password } = this.loginForm.value;
    this.authService.login(email!, password!).pipe(
      this.toast.observe({
        success: 'Inicio de Sesion Correcto :)',
        loading: 'Ingresando...',
        error: 'Ha ocurrido un Error'
      })
    ).subscribe(()=>{
      this.router.navigate(['/inicio']);
    });
  }

}
