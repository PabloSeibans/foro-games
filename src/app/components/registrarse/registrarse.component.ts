import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { AutenticacionService } from '../../services/autenticacion.service';
import { HotToastService } from '@ngneat/hot-toast';
import { Router } from '@angular/router';

export function passwordsMatchValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const password = control.get('password')?.value;
    const confirmPassword = control.get('confirmPassword')?.value;

    if(password && confirmPassword && password !== confirmPassword){
      return {
        passwordsDontMatch: true
      }
    }
    // Cuando no hay ningun error y los passwords sean iguales devuelve true
    return null;
  };
}

@Component({
  selector: 'app-registrarse',
  templateUrl: './registrarse.component.html',
  styleUrls: ['./registrarse.component.css']
})
export class RegistrarseComponent implements OnInit {

  registroForm = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', Validators.required),
    confirmPassword: new FormControl('', Validators.required)
  }, {validators: passwordsMatchValidator()})

  constructor(private authService: AutenticacionService, private toast: HotToastService, private router: Router) { }

  ngOnInit(): void {
  }

  get name() { 
    return this.registroForm.get('name');
   }
   get email() {
    return this.registroForm.get('email');
   }

   get password() {
    return this.registroForm.get('password');
   }

   get confirmPassword() {
    return this.registroForm.get('confirmPassword');
   }


   submit() {
    if(!this.registroForm.valid){
      return;
    }
    // const name: string = this.registroForm.value.name || '';
    // const email: string = this.registroForm.value.email || '';
    // const password: string  = this.registroForm.value.password || '';


    const { name, email, password } = this.registroForm.value;

    this.authService.signUp(name!, email!, password!).pipe(
      this.toast.observe({
        success: 'Felicidades! Estas Registrado',
        loading: 'Registrando...',
      })
    ).subscribe(()=>{
      this.router.navigate(['/inicio'])
    })
  }
}
